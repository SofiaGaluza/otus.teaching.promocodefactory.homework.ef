﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
	public class DataContext : DbContext
	{
		public DataContext(DbContextOptions<DataContext> options) : base(options)
		{
		}
		public DbSet<Employee> Employees { get; set; }
		public DbSet<Role> Roles { get; set; }
		public DbSet<Customer> Customers { get; set; }
		public DbSet<Preference> Preferences { get; set; }
		public DbSet<PromoCode> PromoCodes { get; set; }

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<CustomerPreference>()
				.HasKey(x => new { x.CustomerId, x.PreferenceId });

			modelBuilder.Entity<CustomerPreference>()
				.HasOne(x => x.Customer)
				.WithMany(x => x.CustomerPreferences)
				.HasForeignKey(x=>x.CustomerId);

			modelBuilder.Entity<CustomerPreference>()
				.HasOne(x => x.Preference)
				.WithMany(x => x.CustomerPreferences)
				.HasForeignKey(x=>x.PreferenceId);
				
			modelBuilder.Entity<Customer>()
				.HasMany(c => c.PromoCodes)
				.WithOne(x => x.Customer)
				.OnDelete(DeleteBehavior.Cascade);

			modelBuilder.Entity<Employee>()
				.Property(x => x.FirstName)
				.HasMaxLength(256);
			modelBuilder.Entity<Employee>()
				.Property(x => x.LastName)
				.HasMaxLength(256);
			modelBuilder.Entity<Employee>()
				.Property(x => x.Email)
				.HasMaxLength(50);

			modelBuilder.Entity<Role>()
				.Property(x => x.Description)
				.HasMaxLength(512);
			modelBuilder.Entity<Role>()
				.Property(x => x.Name)
				.HasMaxLength(256);

			modelBuilder.Entity<Customer>()
				.Property(x => x.FirstName)
				.HasMaxLength(256);
			modelBuilder.Entity<Customer>()
				.Property(x => x.LastName)
				.HasMaxLength(256);
			modelBuilder.Entity<Customer>()
				.Property(x => x.Email)
				.HasMaxLength(50);

			modelBuilder.Entity<Preference>()
				.Property(x => x.Name)
				.HasMaxLength(256);

			modelBuilder.Entity<PromoCode>()
				.Property(x => x.Code)
				.HasMaxLength(256);
			modelBuilder.Entity<PromoCode>()
				.Property(x => x.ServiceInfo)
				.HasMaxLength(512);
			modelBuilder.Entity<PromoCode>()
				.Property(x => x.PartnerName)
				.HasMaxLength(256);

		}
	}
}