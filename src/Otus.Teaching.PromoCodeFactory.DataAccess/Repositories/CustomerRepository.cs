﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
	public class CustomerRepository : EfRepository<Customer>
	{
		public CustomerRepository(DataContext dataContext)
			: base(dataContext)
		{
		}

		public async Task<Customer> GetCustomer(Guid id)
		{
			return await _dataContext.Set<Customer>()
				.Include(x=>x.PromoCodes)
				.Include(x=>x.CustomerPreferences)
				.ThenInclude(x=>x.Preference)
				.FirstOrDefaultAsync(x => x.Id == id);
		}

		public override async Task<IEnumerable<Customer>> GetAllAsync()
		{
			return await _dataContext.Set<Customer>()
				.Include(x => x.CustomerPreferences)
				.ThenInclude(x => x.Preference).ToListAsync();
		}
	}
}