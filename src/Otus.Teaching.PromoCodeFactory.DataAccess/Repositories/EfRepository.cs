﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
	public class EfRepository<T>
		: IRepository<T>
		where T : BaseEntity
	{
		protected readonly DataContext _dataContext;

		public EfRepository(DataContext dataContext)
		{
			_dataContext = dataContext;
		}

		public virtual async Task<IEnumerable<T>> GetAllAsync()
		{
			return await _dataContext.Set<T>().ToListAsync();
		}

		public async Task<T> GetByIdAsync(Guid id)
		{
			return await _dataContext.Set<T>().FirstOrDefaultAsync(x => x.Id == id);
		}

		public async Task AddAsync(T entity)
		{
			await _dataContext.Set<T>().AddAsync(entity);

			await _dataContext.SaveChangesAsync();
		}

		public async Task UpdateAsync(T entity)
		{
			_dataContext.Set<T>().Update(entity);

			await _dataContext.SaveChangesAsync();
		}

		public async Task DeleteAsync(T entity)
		{
			_dataContext.Remove(entity);

			await _dataContext.SaveChangesAsync();
		}

		public async Task<IEnumerable<T>> GetRangeAsync(IEnumerable<Guid> ids)
		{
			return await _dataContext.Set<T>().Where(e => ids.Contains(e.Id)).ToListAsync();
		}
	}
}