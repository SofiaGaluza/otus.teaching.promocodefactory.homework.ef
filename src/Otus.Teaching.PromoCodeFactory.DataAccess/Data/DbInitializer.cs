﻿using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
	public class DbInitializer
	{
		private readonly DataContext _dbContext;

		public DbInitializer(DataContext dbContext)
		{
			_dbContext = dbContext;
		}

		public void Initialize()
		{
			_dbContext.AddRange(FakeDataFactory.Employees);
			_dbContext.SaveChanges();

			_dbContext.AddRange(FakeDataFactory.Preferences);
			_dbContext.SaveChanges();

			_dbContext.AddRange(FakeDataFactory.Customers);
			_dbContext.SaveChanges();
		}
	}
}