﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private IRepository<PromoCode> _promoCodeRepository;
        private readonly CustomerRepository _customerRepository;

        public PromocodesController(IRepository<PromoCode> promoCodeRepository, CustomerRepository customerRepository)
        {
	        _promoCodeRepository = promoCodeRepository;
	        _customerRepository = customerRepository;
        }


        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
	        var promoCodes = await _promoCodeRepository.GetAllAsync();
	        var promoCodeShortResponse = promoCodes.Select(p => new PromoCodeShortResponse()
	        {
		        Id = p.Id,
		        Code = p.Code,
		        BeginDate = p.BeginDate.ToString(CultureInfo.InvariantCulture),
		        EndDate = p.EndDate.ToString(CultureInfo.InvariantCulture),
		        PartnerName = p.PartnerName,
		        ServiceInfo = p.ServiceInfo
	        });
	        return Ok(promoCodeShortResponse);

        }

        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
	        var customers = (await _customerRepository.GetAllAsync()).ToList();

			foreach (var customer in customers.Where(c => c.CustomerPreferences
				         .Any(p => p.Preference.Name == request.Preference)))
	        {
		        await _promoCodeRepository.AddAsync(new PromoCode
		        {
			        Id = Guid.NewGuid(),
			        BeginDate = DateTime.UtcNow,
			        EndDate = DateTime.UtcNow.AddDays(30),
			        PartnerName = request.PartnerName,
			        Code = request.PromoCode,
			        ServiceInfo = request.ServiceInfo,
			        Customer = customer
		        });
	        }

	        return Ok();


        }
    }
}