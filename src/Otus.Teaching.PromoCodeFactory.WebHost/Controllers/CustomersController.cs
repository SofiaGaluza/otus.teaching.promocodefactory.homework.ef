﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly CustomerRepository _customerRepository;

        public CustomersController(CustomerRepository customerRepository)
        {
	        _customerRepository = customerRepository;
        }

        /// <summary>
	    /// Получить данные всех клиентов
	    /// </summary>
	    /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IList<CustomerShortResponse>>> GetCustomersAsync()
	    {
		    var  customers = await _customerRepository.GetAllAsync();

		    var customerModelList = customers.Select(x =>
			    new CustomerShortResponse()
			    {
				    Id = x.Id,
                    FirstName = x.FirstName,
                    LastName = x.LastName,
                    Email = x.Email
			    }).ToList();

		    return customerModelList;
        }

        /// <summary>
        /// Получить клиента вместе с выданными ему промомкодами
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await _customerRepository.GetCustomer(id);

            if (customer == null)
	            return NotFound();

            var customerModel = new CustomerResponse()
            {
                Id = customer.Id,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Email = customer.Email,
                PromoCodes = customer.PromoCodes.Select(x=> new PromoCodeShortResponse()
                {
	                Id = x.Id,
                    Code = x.Code,
                    BeginDate = x.BeginDate.ToString(CultureInfo.InvariantCulture),
                    EndDate = x.EndDate.ToString(CultureInfo.InvariantCulture),
                    PartnerName = x.PartnerName,
                    ServiceInfo = x.ServiceInfo
                }).ToList(), 
                PreferenceResponses = customer.CustomerPreferences.Select(x => new PreferenceResponse()
                {
                    Id = x.PreferenceId,
                    Name = x.Preference.Name

                }).ToList()
            };
            
            return customerModel;
        }

        /// <summary>
        /// Создание нового клиента вместе с его предпочтениями.
        /// </summary>
        /// <param name="request">Новый клиент с предпочтениями.</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
	        if (request == null)
	        {
		        throw new ArgumentNullException(nameof(request));
	        }

	        var customer = new Customer
	        {
		        Id = new Guid(),
		        FirstName = request.FirstName,
		        LastName = request.LastName,
		        Email = request.Email,
                CustomerPreferences = request.PreferenceIds.Select(id => new CustomerPreference()
		        {
			        PreferenceId = id
		        }).ToList()
            };
            
            await _customerRepository.AddAsync(customer);

            return Ok(customer.Id);
        }

        /// <summary>
        /// Обновление данных клиента вместе с его предпочтениями.
        /// </summary>
        /// <param name="id">Id клиента.</param>
        /// <param name="request">Измененный клиент с предпочтениями.</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
	        var customer = new Customer
	        {
		        Id = id,
		        LastName = request.LastName,
		        FirstName = request.FirstName,
		        Email = request.Email,
		        CustomerPreferences = request.PreferenceIds.Select(prefId => new CustomerPreference()
		        {
			        PreferenceId = prefId
                }).ToList()
	        };

	        await _customerRepository.UpdateAsync(customer);

	        return Ok();
	    }

        /// <summary>
        /// Удаление клиента вместе с выданными ему промокодами.
        /// </summary>
        /// <param name="id">Id клиента.</param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
	        var customer = _customerRepository.GetCustomer(id).Result;

	        if (customer == null)
		        return NotFound();

            await _customerRepository.DeleteAsync(customer);

	        return NoContent();
        }
    }
}